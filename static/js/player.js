(function(){
var SAMPLERATE = 22050*2;

var bytes = new ArrayBuffer(SAMPLERATE*8);
var samples = new Uint8Array(bytes);

var prv_bytes = new ArrayBuffer(SAMPLERATE/4);
var prv_samples = new Uint8Array(prv_bytes);

var nextcurrow = -1;
var nextcurrow2 = -1;
var looprow = 0;
var looptimes = -1;

// [255, 0] -> "%FF%00"
function b(values) {
    var out = "";
    for (var i = 0; i < values.length; i++) {
        var hex = values[i].toString(16);
        if (hex.length == 1) hex = "0" + hex;
        out += "%" + hex;
    }
    return out.toUpperCase();
}

// Character to ASCII value, or string to array of ASCII values.
function c(str) {
    if (str.length == 1) {
        return str.charCodeAt(0);
    } else {
        var out = [];
        for (var i = 0; i < str.length; i++) {
            out.push(c(str[i]));
        }
        return out;
    }
}

function split32bitValueToBytes(l) {
    return [l&0xff, (l&0xff00)>>8, (l&0xff0000)>>16, (l&0xff000000)>>24];
}


function FMTSubChunk(channels, bitsPerSample, frequency) {
    var byteRate = frequency * channels * bitsPerSample/8;
    var blockAlign = channels * bitsPerSample/8;
    return [].concat(
        c("fmt "),
        split32bitValueToBytes(16), // Subchunk1Size for PCM
        [1, 0], // PCM is 1, split to 16 bit
        [channels, 0], 
        split32bitValueToBytes(frequency),
        split32bitValueToBytes(byteRate),
        [blockAlign, 0],
        [bitsPerSample, 0]
    );
}

function sampleArrayToData(sampleArray, bitsPerSample) {
    var blah = [];
    blah[sampleArray.length - 1] = null;
    for(var i=0;i<sampleArray.length;i++) blah[i]=sampleArray[i];
    return blah;
    if (bitsPerSample === 8) return sampleArray;
    if (bitsPerSample !== 16) {
        alert("Only 8 or 16 bit supported.");
        return;
    }
    
    var data = [];
    for (var i = 0; i < sampleArray.length; i++) {
        data.push(0xff & sampleArray[i]);
        data.push((0xff00 & sampleArray[i])>>8);
    }
    return data;
}

function dataSubChunk(channels, bitsPerSample, sampleArray) {
    return [].concat(
        c("data"),
        split32bitValueToBytes(sampleArray.length * bitsPerSample/8),
        sampleArrayToData(sampleArray, bitsPerSample)
    );
}

function chunkSize(fmt, data) {
    return split32bitValueToBytes(4 + (8 + fmt.length) + (8 + data.length));
}
    
function RIFFChunk(channels, bitsPerSample, frequency, sampleArray) {
    var fmt = FMTSubChunk(channels, bitsPerSample, frequency);
    var data = dataSubChunk(channels, bitsPerSample, sampleArray);
    var header = [].concat(c("RIFF"), chunkSize(fmt, data), c("WAVE"));
    return [].concat(header, fmt, data);
}

function makeURL() {
    var bitsPerSample = 8;    
    var frequency = SAMPLERATE;
    var channels = 1;
    
    return "data:audio/x-wav," + b(RIFFChunk(channels, bitsPerSample, frequency, samples));    
}
function makepURL() {
    var bitsPerSample = 8;    
    var frequency = SAMPLERATE;
    var channels = 1;
    
    return "data:audio/x-wav," + b(RIFFChunk(channels, bitsPerSample, frequency, prv_samples));    
}


window.webkitPlaybackRow = 0;
window.shouldStop = true;

window.generate=function(r) {
  rowTimes = [];
  totalTime = 0;
  row=parseInt(r)||0;
  sample=0;
  tick=0;
  song_end=0;
  if(window.webkitAudioContext && window.shouldStop == false) return;
  
  window.shouldStop = false;
  if(window.webkitAudioContext) {
	  if(!window.context) context = new webkitAudioContext();
	  var node = context.createJavaScriptNode(8192, 0, 1);
	  SAMPLERATE = context.sampleRate;
	  set_the_tempo(THE_TEMPO);
//	  SAMPLES_PER_TICK = Math.floor(SAMPLERATE / 50);
	  setInterval(function() { window.webkitPlaybackRow = Math.floor((row+window.webkitPlaybackRow)/2); }, Math.floor(SOME_SHIT*(8192/SAMPLERATE)));
	  node.onaudioprocess = function (e) {
	  	//window.webkitPlaybackRow = row;
	  	var buf1 = e.outputBuffer.getChannelData(0);
	  	//var buf2 = e.outputBuffer.getChannelData(1);
	  	for(var i=0;i<buf1.length;i++) buf1[i] = (mix()-128)/128;
	  	
	  	
	  	if(window.shouldStop) { node.disconnect(); }
	  }
	  node.connect(context.destination);
  }
  else {
	for(var i=0;i<samples.length;i++) {
      samples[i] = mix();
  	}
  

  
  	$('#audioelt').attr('src', makeURL());
  }
}

window.previewNote = function(name) {
  var num = nameToNum(name);
  if(num == 254) return;
  var phase = 0;
  var samplerate = 8372 * Math.pow(2.0, (num - 12*5) / 12.0);
  for(var i=0;i<prv_samples.length;i++) {
      var left = 0;
  			left += waves[ lastInst ][ Math.floor(phase) ] * 64 / 64.0 / 4;
			

			
			phase += samplerate / SAMPLERATE;
			while(phase >= waves[lastInst].length) {
					phase -= waves[lastInst].length;
					if(!shouldLoop[lastInst]) phase = waves[lastInst].length - 1;
			}  
			
			prv_samples[i] = 128 + 127*left;
  }
  $('#audioelt').attr('src', makepURL());
  
}



//// assynth!

var THE_TEMPO = 125;

var SAMPLES_PER_TICK = Math.floor(2.5 / THE_TEMPO * SAMPLERATE);

var SOME_SHIT = Math.floor(SAMPLERATE / SAMPLES_PER_TICK);


function set_the_tempo(new_tempo) {
	THE_TEMPO = new_tempo;
	SAMPLES_PER_TICK = Math.floor(2.5 / THE_TEMPO * SAMPLERATE);
	SOME_SHIT = Math.floor(SAMPLERATE / SAMPLES_PER_TICK);
}

/* playback state */

function struct_channel() {
return {
 phase:0,
	 note:0,
	 mask:0,
	 wave:0,
	 volume:0,
	 ramp_volume:0,
	 fx:0,
	 param:0,
	 vibrato:0,
	 vibrato_phase:0,
	 porta_dest:0,
	 memory:{},
	 detune:0,
	 chvol:64,
	};
}


var sample=0;
var tick=0;
var row=0;
var song_end=0;
var speed = 3;

var ch = [];

function MatchChannelCount() {
	while (ch.length < CHANNELS) ch.push(struct_channel());
}

function NoiseFunc() {
  var n = [];
  for(var i=0;i<32767;i++)
              n[i] = Math.round(Math.random()) - 0.5;
  return n;
}
var waves = [
  [1,0,0,0,0,0,0,0],
  [1,1,0,0,0,0,0,0],
  [1,1,1,1,0,0,0,0],
  [1,1,1,1,1,1,0,0],
  [0,0.1,0.2,0.3,0.4,0.5,0.6,0.7,0.8,0.7,0.6,0.5,0.4,0.3,0.2,0.1],
  NoiseFunc(),
  [-0.0078, -0.83, -0.99, -0.66, -0.4, 0, 0.39, 0.76, 0.99, 0.99, 0.99, 0.99, 0.99, 0.93, 0.55, 0.44, 0.016, -0.37, -0.73, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.84, -0.59, -0.2, 0.2, 0.59, 1, 0.99, 0.99, 1, 0.99, 1, 1, 0.99, 0.99, 0.99, 0.99, 0.89, 0.81, 0.58, 0.41, 0.16, -0.21, -0.63, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.93, -0.77, -0.66, -0.27, -0.031, 0.39, 0.77, 0.88, 0.99, 0.99, 0.99, 0.91, 0.96, 0.88, 0.99, 0.99, 0.88, 0.69, 0.66, 0.23, -0.14, -0.53, -0.51, -0.66, -0.99, -0.99, -0.91, -0.99, -0.99, -0.91, -0.95, -0.93, -0.91, -0.79, -0.92, -0.77, -0.68, -0.52, -0.55, -0.39, -0.023, 0.031, 0.25, 0.23, 0.45, 0.64, 0.76, 0.91, 0.99, 0.99, 0.89, 0.99, 1, 0.88, 1, 0.92, 0.99, 0.94, 0.91, 0.98, 0.88, 0.71, 0.73, 1, 0.88, 1, 0.81, 0.83, 0.94, 0.77, 0.67, 0.65, 0.63, 0.54, 0.42, 0.32, 0.25, 0.094, -0.086, -0.3, -0.34, -0.48, -0.48, -0.73, -0.88, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.91, -0.016, 0],
  [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0.52, 0.47, 0.055, -0.84, -0.99, -0.99, -0.99, -0.99, -0.91, -0.99, -0.78, 0.63, -0.54, 0.42, 1, 1, 0.32, -0.16, -0.47, -0.75, -0.99, -0.63, -0.77, -0.57, -0.9, -0.5, -0.59, -0.66, 0.34, 0.27, -0.66, -0.99, -0.66, 0.78, 0.41, 0.27, 0.13, -0.0078, 1, 1, 1, 1, 0.85, 0.26, 0.45, 0.66, 0.55, 0.3, -0.29, -0.41, -0.77, 0.19, 0.9, -0.17, 0.094, -0.62, 1, 0.68, 0.87, -0.19, -0.2, -0.29, -0.87, -0.72, -0.32, -0.59, -0.87, -0.99, -0.57, 0.0078, 0.59, 0.55, 0.65, 1, 1, 1, 1, 1, 1, 1, 0.53, -0.78, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, -0.99, 0.41, 1, 1, 1, 0.22, 1, 0.38, 0.63, 0.37, -0.17, -0.44, -0.85, -0.69, -0.39, -0.93, -0.45, -0.88, -0.96, -0.16, -0.48, -0.93, -0.84, -0.99, -0.52, 0.0078, -0.078, 0.0078, 0.0078, 0.48, 0.88, 1, 0.07, 0.57, 0.3, -0.078, -0.5, -0.66, -0.77, -0.19, -0.23, -0.37, -0.65, 0.13, 0.27, -0.039, 0.38, 0.93, -0.59, 0.039, -0.094, -0.23, 0.11, 0.53, -0.26, -0.48, -0.57, -0.59, -0.99, -0.29, -0.99, -0.95, -0.35, 0.65, 1, 0.98, 0.7, 1, 0.48, 0.37, 0.85, 0.55, 0.62, 0.3, -0.85, -0.3, -0.039, -0.14, 0.039, -0.85, -0.5, -0.039, -0.16, 0.19, 0.29, 0.55, -0.023, -0.29, 0.055, -0.44, 0.023, -0.39, -0.99, -0.99, -0.99, -0.84, -0.3, -0.23, -0.45, 0.26, -0.99, 0.3, 0.2, -0.094, -0.41, 0.023, -0.5, 0.44, 0.24, 0.7, 1, 1, 1, 1, 1, 0.41, 0.73, 0.55, 0.023, -0.99, -0.5, -0.99, -0.99, -0.57, -0.99, -0.57, -0.99, -0.99, -0.99, -0.75, -0.26, -0.44, -0.85, -0.7, -0.17, -0.44, 0.26, -0.37, 0.9, 0.24, 1, 0.22, 1, 0.8, 0.72, 1, -0.23, 0.41, 0.3, 0.91, 0.87, 0.57, 0.63, -0.023, 0.42, -0.27, 0.29, -0.72, -0.41, -0.5, -0.55, -0.85, -0.63, -0.055, -0.65, -0.094, -0.094, 0.094, -0.98, -0.32, -0.69, 0.07, 0.13, -0.13, -0.094, 0.47, -0.039, 0.66, 0.41, 0.44, 0.7, 0.78, 1, 0.55, 0.72, 0.78, 0.59, -0.11, -0.0078, 0.68, 0.27, 0.094, 0.34, 0.37, 1, 0.72, -0.29, 0.055, -0.73, -0.48, -0.5, -0.69, -0.99, -0.54, -0.42, -0.72, -0.8, -0.85, -0.078, 0.07, 0.0078, 0.24, 0.35, -0.16, -0.14, -0.27, 0.24, 0.6, -0.19, 0.66, 0.47, 0.32, 0.65, 0.85, 0.38, 0.52, -0.32, 0.32, 0.22, 0.35, 0.38, 0.13, 0.68, 0.66, 0.85, -0.32, -0.3, -0.73, -0.99, 0.023, -0.78, -0.42, -0.77, -0.66, -0.41, -0.8, -0.078, -0.17, -0.42, 0.16, -0.078, -0.42, 0.19, 0.44, 0.48, 0.52, 0.88, 0.41, 0.87, 0.29, 0.83, -0.16, 0.0078, 0.19, -0.2, 0.57, 0.16, 0.14, 0.16, -0.039, 0.24, 0.07, 0.13, -0.34, 0.9, 0.26, 0.3, 0.2, -0.2, -0.055, -0.023, -0.19, -0.99, -0.99, -0.72, -0.99, -0.99, -0.63, -0.41, -0.13, 0.16, 0.91, 0.81, 1, 1, 0.73, 0.85, 0.52, 0.63, 0.29, -0.57, -0.26, 0.023, 0.34, 0.023, 0.29, -0.055, -0.11, 0.37, 0.16, -0.023, 0.16, -0.14, -0.29, 0.055, -0.14, -0.39, -0.19, -0.39, -0.47, -0.44, -0.19, -0.078, -0.9, -0.55, -0.23, -0.7, 0.19, -0.19, -0.2, 0.41, 0.35, 0.66, 1, 1, 0.87, 1, 1, 1, 0.57, -0.0078, 0.44, -0.094, -0.23, 0.094, -0.52, -0.85, -0.73, -0.85, -0.44, 0.07, -0.41, -0.24, -0.35, -0.52, -0.69, -0.34, -0.63, -0.39, -0.16, -0.16, 0.023, 0.16, 0.039, 0.32, 0.0078, 0.055, 0.17, 0.73, 0.37, 0.87, 0.29, 0.96, 0.59, 0.3, 1, 0.93, 0.22, 0.13, 0.3, 0.16, -0.27, -0.41, -0.5, -0.35, 0.039, -0.72, -0.27, -0.63, -0.47, -0.16, -0.34, -0.35, -0.26, -0.078, -0.23, 0.13, 0.11, 0.19, 0.07, 0.53, 0.07, 1, 0.81, 0.81, 0.26, 0.22, 0.14, -0.023, -0.13, -0.37, -0.29, -0.24, -0.039, -0.52, -0.16, 0.22, 0.14, 0.13, 0.3, 0.11, 0.17, 0.55, -0.039, 0.19, 0],
  [0,0.05,0.1,0.15,0.2,0.25,0.3,0.35,0.4,0.45,0.5,0.55,0.6,0.65,0.7,0.75],
  [0.5,0.69,0.85,0.96,1.0,0.96,0.85,0.69,0.5,0.31,0.15,0.04,0.0,0.04,0.15,0.31]  
];
var shouldLoop = [
	true,
	true,
	true,
	true,
	true,
	true,
	false,
	false,
	true,
	true
];

/*struct {
	int length;
	int c5;
	int loop_start;
	int loop_end;
} wave_info[32];
unsigned char *patterns[256];*/

/*
rows = 
[
 [["C-5","1","64","G","A0"],["C-5","1","64","G","A0"]]
]
*/


function generateMask(cells,row,ch) {

  var mask = 0;
  if(cells[0] != '...') mask |= 1;
  if(cells[1] != '.') mask |= 2;
  if(cells[2] != '..') mask |= 4;
  if(cells[3] != '.') mask |= 8;

  return mask;
}

function do_row() {
  MatchChannelCount();
  var rowelt = currentPatternData[row]
  if(row >= rowTimes.length) rowTimes[row] = totalTime;
	for(var channel=0;channel<CHANNELS;channel++) {
    var cells = rowelt[channel];
    
    var mask = generateMask(cells, row, channel);

		ch[channel].mask = mask;
		
		//fprintf(stderr, "ch %d, mask %d\n", channel, mask);
		if(mask & 1) {
		  if((mask & 8) && cells[3] == 'G')
		    ch[channel].porta_dest = nameToNum(cells[0])
			else
        ch[channel].note = nameToNum(cells[0]);
			if(ch[channel].note == 254) ch[channel].volume = 0;
		}
		
		if(mask & 2) {
			ch[channel].wave = parseInt(cells[1]);
			ch[channel].phase = 0;
			ch[channel].volume = 64;
		}

		if(mask & 4) {
			ch[channel].volume = parseInt(cells[2]);
		}
		
		if(mask & 8) {
			 ch[channel].fx = cells[3];
			 ch[channel].param = parseInt(cells[4],16)||0;

			 if (ch[channel].fx == 'Y') {
			 	ch[channel].memory['Y'] = ch[channel].param;
			 }
			 else if (ch[channel].fx == 'S') {

			 }
			 else if (ch[channel].fx != 'C') {

			 if(ch[channel].param != 0) ch[channel].memory[ch[channel].fx] = ch[channel].param;
       else ch[channel].param = ch[channel].memory[ch[channel].fx];
             }
		}

		
		// don't ramp volume on sample start
		if(ch[channel].mask & 2) ch[channel].ramp_volume = ch[channel].volume;
		




	}
}

function do_tick() {
	for(var i=0;i<CHANNELS;i++) {


		if (ch[i].memory['Y'] > 0) {
		   ymem = ch[i].memory['Y'];
		   ylo = ymem & 0x0F;
		   yhi = (ymem & 0xF0) >> 4;
				if(tick == 0) {
					if(yhi == 0xF) {
						if(ylo == 0) ch[i].volume += 15;
						else ch[i].volume -= ylo;
					}
					else if(ylo == 0xF) {
						if(yhi == 0) ch[i].volume -= 15;
						else ch[i].volume += yhi;
					}
				}
				else {
					if(yhi == 0) ch[i].volume -= ylo;
					else if(ylo == 0) ch[i].volume += yhi;
				}
				if(ch[i].volume < 0) ch[i].volume = 0;
				if(ch[i].volume > 64) ch[i].volume = 64;
		}


		if(!(ch[i].mask & 8) || !(ch[i].fx)) {
			ch[i].vibrato = 0;
			ch[i].vibrato_phase = 0;
			continue;
		}
		if(ch[i].fx != 'H' && ch[i].fx != 'K') ch[i].vibrato = ch[i].vibrato_phase = 0;
		
		var param = ch[i].param; // implement effect memory?
		var lo = param & 0x0F;
		var hi = (param & 0xF0) >> 4;



		switch(ch[i].fx) {
			case 'A':
				speed = ch[i].param;
				break;
			case 'T':
				new_tempo = ch[i].param;

				if (new_tempo < 32) { //tempo slide
					if (tick != 0) {
						if (hi == 0) {
							new_tempo = THE_TEMPO - ch[i].param;
						}
						else if (hi == 1) {
							new_tempo = THE_TEMPO + ch[i].param;
						}
						else {
							new_tempo = THE_TEMPO;
						}
					}
					else {
						new_tempo = THE_TEMPO;
					}
				}
				set_the_tempo(new_tempo);
				break;
			case 'C':
				if (tick == 0) {
					nextcurrow = ch[i].param;
				}
				break;
			case 'S':
				if (tick == 0) {
					if (hi == 0xB) {
						if (lo == 0) {
							looprow = row;
						}
						else {
							if (looptimes < 0) {
								looptimes = lo - 2;
								nextcurrow2 = looprow;
							}
							else {
								looptimes--;							
								if (looptimes > -1) nextcurrow2 = looprow;
							}
						}
					}
				}
				break;

			case 'D': 
				if(tick == 0) {
					if(hi == 0xF) {
						if(lo == 0) ch[i].volume += 15;
						else ch[i].volume -= lo;
					}
					else if(lo == 0xF) {
						if(hi == 0) ch[i].volume -= 15;
						else ch[i].volume += hi;
					}
				}
				else {
					if(hi == 0) ch[i].volume -= lo;
					else if(lo == 0) ch[i].volume += hi;
				}
				if(ch[i].volume < 0) ch[i].volume = 0;
				if(ch[i].volume > 64) ch[i].volume = 64;
				break;
			case 'P':
				ch[i].detune = (ch[i].param - 64) / 64;
				break;
			case 'E':
				if(tick != 0)
					ch[i].note -= 4*ch[i].param / 64;
				break;
			case 'F':
				if(tick != 0)
					ch[i].note += 4*ch[i].param / 64;
				break;
			case 'G':
				if(tick != 0) {
				  var slidespeed = 4*ch[i].param / 64.0;
					if(ch[i].porta_dest > ch[i].note + slidespeed)
						ch[i].note += slidespeed;
					else if(ch[i].porta_dest < ch[i].note - slidespeed)
						ch[i].note -= slidespeed;
					else
						ch[i].note = ch[i].porta_dest;
				}
				break;
			case 'K': //H00 + Dxy
				if(tick == 0) {
					if(hi == 0xF) {
						if(lo == 0) ch[i].volume += 15;
						else ch[i].volume -= lo;
					}
					else if(lo == 0xF) {
						if(hi == 0) ch[i].volume -= 15;
						else ch[i].volume += hi;
					}
				}
				else {
					if(hi == 0) ch[i].volume -= lo;
					else if(lo == 0) ch[i].volume += hi;
				}
				if(ch[i].volume < 0) ch[i].volume = 0;
				if(ch[i].volume > 64) ch[i].volume = 64;

				vmem = ch[i].memory['H'];
				vlo = vmem & 0x0F;
				vhi = (vmem & 0xF0) >> 4;
				ch[i].vibrato = 1.0*vlo*64.0*Math.sin(2*3.141593/192*ch[i].vibrato_phase)/768.0;
				ch[i].vibrato_phase += 4*vhi;
				break;

			case 'L': //G00 + Dxy
				if(tick == 0) {
					if(hi == 0xF) {
						if(lo == 0) ch[i].volume += 15;
						else ch[i].volume -= lo;
					}
					else if(lo == 0xF) {
						if(hi == 0) ch[i].volume -= 15;
						else ch[i].volume += hi;
					}
				}
				else {
					if(hi == 0) ch[i].volume -= lo;
					else if(lo == 0) ch[i].volume += hi;
				}
				if(ch[i].volume < 0) ch[i].volume = 0;
				if(ch[i].volume > 64) ch[i].volume = 64;

				gmem = ch[i].memory['G'];
				if(tick != 0) {
				  var slidespeed = 4*gmem / 64.0;
					if(ch[i].porta_dest > ch[i].note + slidespeed)
						ch[i].note += slidespeed;
					else if(ch[i].porta_dest < ch[i].note - slidespeed)
						ch[i].note -= slidespeed;
					else
						ch[i].note = ch[i].porta_dest;
				}
				break;


			case 'H':
				ch[i].vibrato = 1.0*lo*64.0*Math.sin(2*3.141593/192*ch[i].vibrato_phase)/768.0;
				ch[i].vibrato_phase += 4*hi;
				break;
			case 'J':
				if(tick % 3 == 1)
					ch[i].vibrato = hi;
				else if(tick % 3 == 2)
					ch[i].vibrato = lo;
				else
					ch[i].vibrato = 0;
				break;
			case 'M':
			   ch[i].chvol = ch[i].param;
			   break;
		}

	}	
}

function mix() {
  totalTime += 1.0/SAMPLERATE;
	//if(row == 0 && tick == 0 && sample == 0)
	//	do_pattern();
	if(tick == 0 && sample == 0)
		do_row();
	if(sample == 0)
		do_tick();
		
	var left = 0;

	for(var i=0;i<CHANNELS;i++) {
		//if(i==3) fprintf(stderr, "channel %d, wave %d, phase %d, vol %d\n", i, ch[i].wave, (int)(ch[i].phase), ch[i].volume);
		if(ch[i].note < 254) {
			if(ch[i].ramp_volume < ch[i].volume) ch[i].ramp_volume+=0.125;
			if(ch[i].ramp_volume > ch[i].volume) ch[i].ramp_volume-=0.125;
			left += waves[ ch[i].wave ][ Math.floor(ch[i].phase) ] * ch[i].ramp_volume / 64.0 / 4 * ch[i].chvol/64.0;
			

			var samplerate = 8372 * Math.pow(2.0, (ch[i].note - 12*5 + ch[i].vibrato + ch[i].detune) / 12.0);
			ch[i].phase += samplerate / SAMPLERATE;
			while(ch[i].phase >= waves[ch[i].wave].length) {

					ch[i].phase -= waves[ch[i].wave].length;
					if(!shouldLoop[ch[i].wave]) ch[i].phase = waves[ch[i].wave].length - 1;

			}
		}
	}
	
	if(left > 1.0) left = 1;
	if(left < -1.0) left = -1;
	
	sample++;
	if(sample >= SAMPLES_PER_TICK) {
		sample = 0;
		tick++;
		if(tick == speed) {
			tick = 0;
			if (nextcurrow2 >= 0) {
				row = nextcurrow2;
				nextcurrow2 = -1;
				nextcurrow = -1;
			}
			else if (nextcurrow >= 0) {
				row = nextcurrow;
				nextcurrow = -1;
			}
			else {
				row++;
			}
			if(row == ROWS) {
				row = 0;
				//order++;
				/*if(orders[order] == 255)*/ song_end = 1;
			}
		}
	}

	//if(song_end==1)return 128;
	return 128+127*left;
}


})();