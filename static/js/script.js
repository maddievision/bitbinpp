currentPatternData=[];

CHANNELS=8;
ROWS=128;

curRow=0;
curCh=0;
curField=0;

curOctave = 4;


curStep = 2;


lastInst = 2;

midi = null
midiInput = null;

function setupMIDI() {
  navigator.requestMIDIAccess( function(midiAccess) { 
    midi = midiAccess;
    midiInputs = midi.getInputs();
    if (midiInputs.length > 0) {
      midiInput = midi.getInput( midiInputs[0] );
      midiInput.addEventListener("message", handleMIDIMessage );      
    }
  }, null);
}

function handleMIDIMessage( ev ) {
  if (ev.data.length == 3) {
    status = ev.data[0];
    shi = (status & 0xF0) >> 4;
    if (shi == 0x9) {
      notenum = ev.data[1];
      velo = ev.data[2];
      if (velo > 0) {
        notebase = noteNames[notenum % 12];
        noteoct = Math.floor(notenum / 12);
        notename = notebase + noteoct;
        enterNoteName(notename); 
      }
    }
  }
}

function buildUI() {
  var s = '<table id="patternData" cellspacing=0 cellpadding=0>';
  s += '<thead><tr><th></th>';
  for(var i=0;i<CHANNELS;i++)
    s += '<th colspan=5>Ch. '+ (i+1) +'</th>';
  s += '</tr></thead>';
  for(var r=0;r<ROWS;r++) {
    s += '<tr>';
    s += '<td class="rownum">' + r + '</td>';
    for(var i=0;i<CHANNELS;i++) {
      s += '<td class="note">...</td>';
      s += '<td class="inst">.</td>';
      s += '<td class="vol">..</td>';
      s += '<td class="fx">.</td>';
      s += '<td class="param">..</td>';
    }
    s += '</tr>'
  }
  $('#pattern').html(s);
  
  var nope = function(e) { e.preventDefault(); return false; }
  $('#pattern').bind('selectstart', nope )
  $('#pattern').bind('dragstart', nope )
  
  updateCursor();
}


function buildUIFromArray() {
  var s = '<table id="patternData" cellspacing=0 cellpadding=0>';
  s += '<thead><tr><th></th>';
  for(var i=0;i<CHANNELS;i++)
    s += '<th colspan=5>Ch. '+ (i+1) +'</th>';
  s += '</tr></thead>';
  for(var r=0;r<ROWS;r++) {
    theRow = currentPatternData[r];
    s += '<tr>';
    s += '<td class="rownum">' + r + '</td>';
    for(var i=0;i<CHANNELS;i++) {
      theCells = theRow[i];
      s += '<td class="note">'+theCells[0]+'</td>';
      s += '<td class="inst">'+theCells[1]+'</td>';
      s += '<td class="vol">'+theCells[2]+'</td>';
      s += '<td class="fx">'+theCells[3]+'</td>';
      s += '<td class="param">'+theCells[4]+'</td>';
    }
    s += '</tr>'
  }
  $('#pattern').html(s);
  
  var nope = function(e) { e.preventDefault(); return false; }
  $('#pattern').bind('selectstart', nope )
  $('#pattern').bind('dragstart', nope )
  $('#pattern table').mousedown(mousedown);
  $('#pattern table').mouseover(mouseover);
  $('#pattern table').mouseup(mouseup);
  
  updateCursor();
}


function startInput() {
  $(window).bind('keydown', keyDown);
  
}

function currentCellData() {
  return currentPatternData[curRow][curCh];
}


function currentCell() {
  return $('#pattern table tr').eq(1+curRow).find('td').eq(1 + curCh*5+curField);
}
function updateCursor() {
  $('#pattern .cursor').removeClass('cursor');
  currentCell().addClass('cursor');
  
  window.scrollTo( 0,  currentCell().offset().top - window.innerHeight/2 );
}

noteMap = "zsxdcvgbhnjmq2w3er5t6y7ui9o0p";
//noteMap = "qwertyuiop[]asdfghjkl;'\zxcvbnm,./"

noteNames = [
"C-",
"C#",
"D-",
"D#",
"E-",
"F-",
"F#",
"G-",
"G#",
"A-",
"A#",
"B-"
];


function cursorLeft() {
  curField--;
  if(curField<0) { 
    curField = 4;
    curCh--;
  }
  if(curCh<0) {
    curCh = CHANNELS-1;
  }
}

function cursorRight() {
  curField++;
  if(curField>4) {
    curField=0;
    curCh++;
  }
  if(curCh>=CHANNELS) {
    curCh = 0;
  }
}

function cursorUp() {
  curRow--;
  if(curRow<0)curRow = ROWS-1;
}
function cursorDown() {
  curRow++;
  if(curRow>=ROWS) curRow = 0;
}

function clearCell() {
  currentCell().html('.');
  if(curField == 0) { currentCell().html('...'); stepForward(); }
  if(curField == 2 || curField == 4) currentCell().html('..')
  currentCellData()[curfield] = currentCell().html();
}
function numToName(num) {
  var oct = Math.floor(num/12);
  var cls = num % 12;
  return noteNames[cls] + oct;
}
function nameToNum(name) {
  if(name == "^^^") return 254;
  var oct = name.substr(2,1);
  var cls = name.substr(0,2);
  return oct * 12 + noteNames.indexOf(cls);
}

function stepForward() {
  for(var i=0;i<curStep;i++)
    cursorDown();
}
function isNoteCode(code) {
  if(code > 90) return false;
  return String.fromCharCode(code) && noteMap.indexOf(String.fromCharCode(code).toLowerCase()) > -1;
}
function enterNote(code) {
  var note = noteMap.indexOf(String.fromCharCode(code).toLowerCase());
  noteNum = curOctave * 12 + note;
  var name = numToName(noteNum);
  if(code == 49) name = '^^^';
  $('#pattern tr').eq(1 + curRow).find('td').eq(1 + curCh*5).html(name);
  currentCellData()[0] = name;  
  curField++; enterInst(lastInst); curField--;
  
  stepForward();
  updateCursor();
  
  previewNote(name);
}

function enterNoteName(name) {
  $('#pattern tr').eq(1 + curRow).find('td').eq(1 + curCh*5).html(name);
  currentCellData()[0] = name;  
  curField++; enterInst(lastInst); curField--;
  stepForward();
  updateCursor();
  previewNote(name);
}


instNames = [
'12.5% Pulse',
'25% Pulse',
'50% Pulse',
'75% Pulse',
'Triangle',
'Noise',
'Kick',
'Snare',
'Saw',
'Sine'
];

function enterInst(n)
{
  currentCell().html(n);
  currentCellData()[1] = currentCell().html();
  lastInst = n;
       $('#instdisp').html(n);
       $('#instname').html(instNames[n]);
}
function enterVol(n)
{
  var v = parseInt(currentCell().html());
  if(!v) v =0;
  v = v*10 + n;
  v = v % 100;
  currentCell().html(v);
  currentCellData()[2] = currentCell().html();
}

function enterFx(c) {
  currentCell().html(String.fromCharCode(65 + c));
  currentCellData()[3] = currentCell().html();
  curField++;
  enterParam(0);
  curField--;
}
function enterParam(d) {
  var v = parseInt(currentCell().html(), 16);
  if(!v) v=0;
  v = v*16 + d;
  v = v % 256;
  var str = v.toString(16).toUpperCase();
  
  if(v < 16) str = '0'+str;
  currentCell().html(str);
  currentCellData()[4] = currentCell().html();
}
function hexKey(code) {
  var a = code - 48;
  if(a>9) a = 10 + code - 65;
  if(a > 15) a = -1;
  return a;
}

function updateMark() {
  $('#pattern td.marked').removeClass('marked');
  if(nullMark()) return;
  var minRow = Math.min(markIn[0],markOut[0]);
  var minCh = Math.min(markIn[1],markOut[1]);
  var minField = Math.min(markIn[2],markOut[2]);
  var maxRow = Math.max(markIn[0],markOut[0]);
  var maxCh = Math.max(markIn[1],markOut[1]);
  var maxField = Math.max(markIn[2],markOut[2]);
  
  var prefilter = $('#pattern table tr:gt(' + (minRow) + '):lt(' + (maxRow-minRow+1) + ') td');
  
  var rowlength = $('#pattern table tr:eq(1) td').size();
  
  for(var i=minRow;i<=maxRow;i++)
    for(var j=minCh;j<=maxCh;j++)
      for(var k=minField;k<=maxField;k++) {
        //$('#pattern table tr:eq(' + (1+i) + ') td:eq(' + (1+j*5+k) + ')').addClass('marked');
        $(prefilter [ ( (i-minRow)*rowlength) + (1+j*5+k) ]).addClass('marked');
      
      }
}

function markChannel() {
  markIn = [0, curCh, 0];
  markOut = [ROWS-1, curCh, 4];
  updateMark();
}

function editCopy() {

  var minRow = Math.min(markIn[0],markOut[0]);
  var minCh = Math.min(markIn[1],markOut[1]);
  var minField = Math.min(markIn[2],markOut[2]);
  var maxRow = Math.max(markIn[0],markOut[0]);
  var maxCh = Math.max(markIn[1],markOut[1]);
  var maxField = Math.max(markIn[2],markOut[2]);
  copyBuffer = { minCh:minCh,maxCh:maxCh, minField:minField, maxField:maxField, data:[] };
  for(var i=minRow;i<=maxRow;i++) {
      copyBuffer.data.push(
        $('#pattern table tr:eq(' + (1+i) + ')').html()
         );
      }
}
function editCut() {
     editCopy();

}

function editPaste() {
  for(var i=0;i<copyBuffer.data.length;i++)
    for(var j=copyBuffer.minCh;j<=copyBuffer.maxCh;j++)
      for(var k=copyBuffer.minField;k<=copyBuffer.maxField;k++) {
        data = copyBuffer.data[i].split('</td>')[1+j*5+k];
        data = data.split('>')[1];
        $('#pattern table tr:eq(' + (1+i+curRow) + ') td:eq(' + (1+j*5+k + 5*(curCh-copyBuffer.minCh) ) + ')').html( data );
      }
}


function transposeOneNote(elt, delta) {
	var name = elt.html();
	var num = nameToNum(name);
	if(!num) return;
	if(num>119) return;
	
	num+=delta; 
	if(num > 119) num = 119;
	if(num < 0) num = 0;
	elt.html(numToName(num));
}

function replaceInstrumentWithLast() {
  if(nullMark()) { 
    var elt = $('#pattern tr').eq(1 + curRow).find('td').eq(2 + curCh*5);
    if (elt.html() != ".") elt.html(lastInst);
  }
  else {
    $('#pattern td.inst.marked').each(function(){ if ($(this).html() != ".") $(this).html(lastInst); });
  }
}

function transposeOctUp() {
  for(var i=0;i<12;i++) transposeUp();
}

function transposeOctDown() {
  for(var i=0;i<12;i++) transposeDown();
}


function transposeUp() {
	if(nullMark()) transposeOneNote($('#pattern tr').eq(1 + curRow).find('td').eq(1 + curCh*5), 1)
	else {
		$('#pattern td.note.marked').each(function(){ transposeOneNote($(this), 1); });
	}
}

function transposeDown() {
	if(nullMark()) transposeOneNote($('#pattern tr').eq(1 + curRow).find('td').eq(1 + curCh*5), -1)
	else {
		$('#pattern td.note.marked').each(function(){ transposeOneNote($(this), -1); });
	}
}

function clearMarked() {
  if (!nullMark()) {
    $('#pattern td.note.marked').html('...');
    $('#pattern td.inst.marked').html('.');
    $('#pattern td.vol.marked').html('..');
    $('#pattern td.fx.marked').html('.');
    $('#pattern td.param.marked').html('..');
  }
}


function keyDown(e) {

if(document.activeElement != document.body) return;

  if(e.keyCode == 37) {
    cursorLeft();
  }
  if(e.keyCode == 38) {
    cursorUp();
  }
  if(e.keyCode == 39) {
    cursorRight();
  }
  if(e.keyCode == 40) {
    cursorDown();
  }
  
  if(e.keyCode == 33) {
  	for(var c=0;c<8;c++)
  	cursorUp();
  }
  if(e.keyCode == 34) {
  	for(var c=0;c<8;c++)
  	cursorDown();
  }
  
   if(e.altKey || e.ctrlKey) {
      e.preventDefault();
      if(e.keyCode >= 48 && e.keyCode <= 57)
         $('#step').html(curStep = e.keyCode-48);
       if(e.keyCode == 88)
        editCut();
        if(e.keyCode == 67)
        editCopy();
        if(e.keyCode == 80)
        editPaste();
        if(e.keyCode == 86 && e.ctrlKey)
        editPaste();
        if(e.keyCode == 76)
        markChannel();
        if(e.keyCode == 36)
        $('#octdn').click();
        if(e.keyCode == 35)
        $('#octup').click();
		if(e.altKey && e.ctrlKey) {
			if(e.keyCode == 81) transposeOctUp();
			if(e.keyCode == 65) transposeOctDown();
		}
		else { 
			if(e.keyCode == 81) 
				transposeUp();
			if(e.keyCode == 65)
				transposeDown();
		} 
   }
   else {
  
    console.log(e.keyCode);
    
        if(e.keyCode == 111) {e.preventDefault(); $('#octdn').click(); }

        if(e.keyCode == 106) {e.preventDefault(); $('#octup').click();   }
        
    if(e.keyCode == 116)
      { e.preventDefault(); }
    if(e.keyCode == 117)
      {e.preventDefault(); generate(0); }
    if(e.keyCode == 118)
      {e.preventDefault(); generate(curRow); }
    if(e.keyCode == 119)
      {e.preventDefault(); shouldStop = true; audioelt.pause(); }
      
    if(e.keyCode==16)
      markIn = [curRow, curCh, curField];
    
    if(curField == 0 && isNoteCode(e.keyCode))
      enterNote(e.keyCode);
    if(curField == 0 && e.keyCode == 49)
      enterNote(e.keyCode);
    if(curField == 1 && e.keyCode >= 48 && e.keyCode <= 57)
      enterInst(e.keyCode-48);
    if(curField == 2 && e.keyCode >= 48 && e.keyCode <= 57)
      enterVol(e.keyCode-48);
    if(curField == 3 && e.keyCode >= 65 && e.keyCode <= 90)
      enterFx(e.keyCode-65);
    if(curField == 4 && hexKey(e.keyCode)>-1)
      enterParam(hexKey(e.keyCode))
      
    if(e.shiftKey) {
      markOut = [curRow, curCh, curField];
      updateMark();
      }
   }
    
  //console.log(String.fromCharCode(e.keyCode))
  
  if(e.keyCode == 32 || e.keyCode == 190)
    clearCell();
  if (e.keyCode == 46)
    clearMarked();

 updateCursor();
     
  if(e.keyCode >= 37 && e.keyCode <= 40)
    e.preventDefault(); 
  if(e.keyCode == 32) e.preventDefault();
  if(e.keyCode == 33) e.preventDefault();
  if(e.keyCode == 34) e.preventDefault();
  
  if(e.keyCode == 8) e.preventDefault(); // bksp
  if(e.keyCode == 45) { e.preventDefault();  markIn = [curRow, curCh, 0];  markOut = [ROWS-1, curCh, 4]; editCopy(); curRow++; editPaste(); curRow--; markOut = [curRow, curCh, 0]; } // crappy insert 
}

function eltToPos(elt) {
	if(elt.tagName != 'TD') return false;
	var r = parseInt(elt.parentElement.childNodes[0].innerHTML);
	if(r >= 0) {}
	else return false;
	var i=0;
	for(;elt.parentElement.childNodes[i] != elt;i++);
	if(i<1) i = 1;
	return [r, Math.floor((i-1)/5), (i-1)%5];
}
/*
function clicked(e)
{

  if(r >= 0) curRow = r;

  curField = (i-1) % 5;
  curCh = Math.floor((i-1)/5);
  
  updateCursor();
  
}*/
function nullMark() {
	return ((!window.markIn || !window.markOut) || (markIn[0] == markOut[0] && markIn[1] == markOut[1] && markIn[2] == markOut[2]));
}
var selecting = false;
function mousedown(e) {
	if(e.which != 1) return;
	selecting = true;
	var pos = eltToPos(e.target);
	if(pos) { e.preventDefault(); markIn = markOut = pos; updateMark(); }
	return;
}
function mouseover(e) {
	if(e.which != 1) return;
	if(!selecting) return;
	var pos = eltToPos(e.target);
	if(pos) { /*e.preventDefault();*/ markOut = pos; updateMark(); }
}
function mouseup(e) {
	var pos = eltToPos(e.target);
	selecting = false;
	if(!window.markIn && pos) { curRow = pos[0]; curCh = pos[1]; curField = pos[2]; updateCursor();  return; }
	if(nullMark()) {
		$('#pattern td.marked').removeClass('marked');
		if(pos) { curRow = pos[0]; curCh = pos[1]; curField = pos[2]; updateCursor(); }
	}
	else {
		if(pos) markOut = pos; updateMark();
	}
}

function playtime(e) {
  if(window.webkitAudioContext && shouldStop) return;
  if(!window.webkitAudioContext && audioelt.paused) return;
  
  if(window.webkitAudioContext)
  	var row = webkitPlaybackRow;
  else
  	for(var i=0;i<rowTimes.length && (!rowTimes[i] || audioelt.currentTime >= rowTimes[i]);i++) 
    	var row = i; //var row = 1 + Math.floor(audioelt.currentTime * SAMPLERATE / SAMPLES_PER_TICK / 3);
  //$('#pattern table tr.playback').removeClass('playback');
  //$('#pattern table tr').eq(1+row).addClass('playback');

  $('#pattern table tr td.playback').removeClass('playback');
  $('#pattern table tr').eq(1+row).find('td').addClass('playback');

}

function spinoct() {
  var name = 'oct';
  
   $('#'+name+'up').click(function() {
       curOctave++;
       if(curOctave > 9) curOctave = 9;
       $('#'+name).html(curOctave);
   });
   $('#'+name+'dn').click(function() {
       curOctave--;
       if(curOctave < 0) curOctave = 0;
       $('#'+name).html(curOctave);
   });
}


function spintransb() {
  var name = 'transb';
  
   $('#'+name+'up').click(function() {
      transposeUp();
   });
   $('#'+name+'dn').click(function() {
      transposeDown();
   });
   $('#'+name+'octup').click(function() {
      transposeOctUp();
   });
   $('#'+name+'octdn').click(function() {
      transposeOctDown();
   });
}

function spininst() {
  var name = 'inst';
  
   $('#'+name+'up').click(function() {
       n = (lastInst + 1) % instNames.length;
       $('#instdisp').html(n);
       $('#instname').html(instNames[n]);
       lastInst = n;
   });
   $('#'+name+'dn').click(function() {
       n = (lastInst - 1);
       if (n < 0) n = instNames.length-1;
       $('#instdisp').html(n);
       $('#instname').html(instNames[n]);
       lastInst = n;
   });
}


function spinstep() {
var name = 'step';
   $('#'+name+'up').click(function() {
       curStep++;
       if(curStep > 9) curStep = 9;
       $('#'+name).html(curStep);
   });
   $('#'+name+'dn').click(function() {
       curStep--;
       if(curStep < 0) curStep = 0;
       $('#'+name).html(curStep);
   });
}
function importbitbin() {

bitbinurl = $("#inputBitbin").val();
parts = bitbinurl.split("/");
songid = parts[parts.length-1];
window.location.href = "/bitbin/" + songid;

}

function resizesong() {
  doStop();
  newrows = $("#inputResizeRows").val();
  newchans = $("#inputResizeChannels").val();
  xrows = ROWS;
  xchans = CHANNELS;
  if (newrows < xrows) { //remove rows
    delta = xrows - newrows;
    currentPatternData.splice(xrows-delta,delta);
    xrows = newrows;
  }
  if (newchans < xchans) { //remove channels
    delta = xchans - newchans;
    for (r = 0; r < xrows; r++) {
      currentPatternData[r].splice(xchans-delta,delta);
    }
    xchans = newchans;
  }
  if (newchans > xchans) { //add channels
    delta = newchans - xchans;
    for ( r = 0; r < xrows; r++) {
      for ( i = 0; i < delta; i++) {        
        currentPatternData[r].push(['...','.','..','.','..']);
      }
    }
    xchans = newchans;
  }
  if (newrows > ROWS) {
    delta = newrows - xrows;
    for ( r = 0; r < delta; r++) {
      theRow = []
      for ( i = 0; i < xchans; i++) {
        theRow.push(['...','.','..','.','..']);        
      }
      currentPatternData.push(theRow);
    }
    xrows = newrows;
  }
  ROWS = xrows;
  CHANNELS = xchans;
  $('#rowcountdisp').html(ROWS);
  $('#chancountdisp').html(CHANNELS);
  buildUIFromArray();
  $('#resizemodal').modal('hide')
}

function newsong() {

numrows = $("#inputRows").val();
numchans = $("#inputChannels").val();
window.location.href = "/new/" + numrows + "/" + numchans;

}
function upload() {

$.post("/upload",{'name':$('#myname').val(), 'title':$('#mytitle').val(), 'data': $('#pattern table').html()}, function( msg ) {
   window.location.href = msg;
});


}

function doStop() {
  shouldStop = true; audioelt.pause();
}


function focusfunc(e) {
  focusedelt = e.target;
}

function init() {
  setupMIDI();
  ROWS = $("#patternData > tbody > tr").length;
  CHANNELS = $("#patternData > thead > tr > th").length - 1;
  $("#inputResizeRows").val(ROWS);
  $("#inputResizeChannels").val(CHANNELS);
  currentPatternData = [];

  for (row = 0; row < ROWS; row++) {
    var rowelt = $('#pattern table tr').eq(1+row).find('td');
    var theRow = [];
    for(var channel=0;channel<CHANNELS;channel++) {
      var cells = rowelt.slice(1+channel*5);
      var theCells = [cells[0].innerHTML,cells[1].innerHTML,cells[2].innerHTML,cells[3].innerHTML,cells[4].innerHTML];
      theRow.push(theCells);
    }
    currentPatternData.push(theRow);
  }

  $('#rowcountdisp').html(ROWS);
  $('#chancountdisp').html(CHANNELS);
  //window.alert("ROWS "+ROWS+" CHANNELS "+CHANNELS);
  if(!$('#pattern table').html())
    buildUI();
  startInput();
  
  $('#save').click(function() {
     sessionStorage.setItem('song', $('#pattern table').html());
  });
  $('#load').click(function() {
    var song = sessionStorage.getItem('song');
    if(song)
      $('#pattern table').html(song);
    else
      alert("nothing to load - local save is only per-session/tab!");
  } );
    
  updateCursor();
  
  $('#repinst').click(replaceInstrumentWithLast);

  $('#play').click(generate);
  $('#stop').click(doStop);
  $('#pattern table').mousedown(mousedown);
  $('#pattern table').mouseover(mouseover);
  $('#pattern table').mouseup(mouseup);
  
  
  spinoct();
  spintransb();
  spininst();
  spinstep();
  
  $('#upload').click(upload);
  $('#createnew').click(newsong);
  $('#importbitbin').click(importbitbin);
  $('#songresize').click(resizesong);

  setInterval(playtime, 100);
  
  
  
  if(window.audioelt==undefined) audioelt=$('#audioelt')[0];
  if(window.webkitAudioContext) { 
  	$('#audioelt').hide();
//  	$('#play').text('Play');
  }
  
  
  $('body').on('selectstart', function(e){e.preventDefault();})
  $('#side').on('selectstart', function(e){e.stopPropagation();})
  
  setTimeout(function(){generate(0)}, 500);
}


$(init);