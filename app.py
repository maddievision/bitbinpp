from flask import Flask, Markup, render_template, request
import sys
import datetime
import uuid
import requests
import json
from pymongo import MongoClient, DESCENDING
from xml.dom.minidom import parseString

CONFIG = {
  'MONGO_SONGS_COLLECTION': 'songs',     
  'MONGO_SETTINGS_COLLECTION': 'settings',     
}

with open("config.json") as f:
  CONFIG = json.loads(f.read())

connection = MongoClient(CONFIG['MONGO_URL'])
db = connection[CONFIG['MONGO_DB']]
collection = db[CONFIG['MONGO_SONGS_COLLECTION']]
bsettings = db[CONFIG['MONGO_SETTINGS_COLLECTION']]

app = Flask(__name__)

def create_pattern_data_json(channels=8,rows=8,data=None):
    output = []
    if not data:
        output = []
        for i in xrange(rows):
            theRow = []
            for j in xrange(channels):
                theRow.append(['...','.','..','.','..'])
            output.append(theRow)
    else:
        output = data
    return json.dumps(output)

def create_pattern_data(channels=8,rows=128,data=None):
    out = '<table id="patternData" cellspacing="0" cellpadding="0">'

    if not data:
        out += '<thead><tr><th></th>'
        for i in xrange(channels):
            out += '<th colspan="5">Ch. %d</th>' % (i+1)
        out += '</tr></thead><tbody><tr class="">'
        for i in xrange(rows):
            out += '<td class="rownum">%d</td>' % i
            for j in xrange(channels):
                out += '<td class="note">...</td><td class="inst">.</td><td class="vol">..</td><td class="fx">.</td><td class="param">..</td>'
            out += '</tr>'
    else:
        out += data
    out += '</tbody></table>'
    return Markup(out)

def get_sample_songs():
    out = ""
    ss = bsettings.find_one({"key":"samples"})
    SAMPLE_SONGS = []
    if ss:
        SAMPLE_SONGS = ss['value']
    for suid in SAMPLE_SONGS:
        song = collection.find_one({"songuuid":suid})
        if song:
            out += '<li><a href="/song/%s">%s by %s</a></li>' % (song['songuuid'],song['title'],song['name'])
    return Markup(out)

def get_last10songs():
    lastten = collection.find(sort=[("date",DESCENDING)],limit=10)
    out = ""
    for song in lastten:
        datefmt = song['date'].strftime("%Y-%m-%d %H:%M:%S")
        out += '<li><a href="/song/%s"><small>%s</small> - %s by %s</a></li>' % (song['songuuid'],datefmt,song['title'],song['name'])
    return Markup(out)

@app.route("/")
def index():
    return render_with_pattern(create_pattern_data())
@app.route("/new/<int:rows>")
def withrows(rows):
    return render_with_pattern(create_pattern_data(rows=rows))
@app.route("/new/<int:rows>/<int:channels>")
def withrowschannels(rows,channels):
    return render_with_pattern(create_pattern_data(channels=channels,rows=rows))

@app.route("/songs/")
def allsongs():
    lastten = collection.find(sort=[("date",DESCENDING)])
    out = ""
    for song in lastten:
        datefmt = song['date'].strftime("%Y-%m-%d %H:%M:%S")
        out += '<small>%s</small> - <a href="/song/%s">%s by %s</a><br>' % (datefmt,song['songuuid'],song['title'],song['name'])
    songlist = Markup(out)
    return render_template('songs.html',songlist=songlist)

def makeuuid():
    return uuid.uuid4().hex[0:8]

@app.route("/upload",methods=["POST","GET"])
def upload():
    xdata = None
    if request.method == "POST":
        xdata = request.form
    else:
        xdata = request.args
    song = {
            "songuuid": makeuuid(),
            "name": xdata['name'],
            "title": xdata['title'],
            "data": xdata['data'],
            "date": datetime.datetime.utcnow()
            }
    collection.insert(song)
    return '/song/%s' % song['songuuid']

@app.route("/bitbin/<songid>")
def bitbinimport(songid):
    r = requests.get('http://bit.s3m.us/%s' % songid)
    if r.status_code == 200:
        lines = r.text.splitlines()
        name = ""
        title = ""
        marked = False
        data = ""
        for line in lines:
            if line.startswith("artist: "):
                qend = line.rfind("\"")
                qstart = line.find("value=\"") + 7
                name = line[qstart:qend]
            elif line.startswith("title: "):
                qend = line.rfind("\"")
                qstart = line.find("value=\"") + 7
                title = line[qstart:qend]
            elif line == '<div id="pattern">':
                marked = True
            elif marked:
                data = line.replace("<table cellspacing=0 cellpadding=0>","").replace("</table>","")
                break
        return render_with_pattern(create_pattern_data(data=data),name=name,title=title)
    else:
        return render_with_pattern(create_pattern_data())




@app.route("/song/<songuuid>")
def opensong(songuuid):
    song = collection.find_one({"songuuid": songuuid})    
    if not song:
        patternData = create_pattern_data()
        name = "Artist"
        title = "Untitled"
    else:
        patternData = create_pattern_data(data=song['data'])
        name = song['name']
        title = song['title']

        scode = '<a href="https://twitter.com/share" class="twitter-share-button" data-text="Check out %s by %s!" data-size="large" data-hashtags="bitbin">Tweet</a>' % (title,name)
        scode += '<script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="//platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");</script>'
        sharecode = Markup(scode)
    return render_with_pattern(patternData,name,title,sharecode)

def render_with_pattern(patternData,name="Artist",title="Untitled",sharecode=""):
    return render_template('main.html',sharecode=sharecode,patternData=patternData,name=name,title=title,sampleSongs=get_sample_songs(),last10Songs=get_last10songs())


if __name__ == "__main__":
   app.run(host="0.0.0.0",debug=True)

